package com.sgh.common.auth.authservice.controllerTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.models.SmUser;
import com.sgh.common.auth.authservice.services.UserService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TestUserController {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService userService;
	
//	@Test
//	public void shouldCreateUserAndReturnSameUser() throws Exception {
//		Department dept = new Department("Solutions");
//		Role role = new Role(ERole.EMPLOYEE);
//		SmUser user = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
//		Mockito.when(userService.createUser(user)).thenReturn(user);
//		
//		this.mockMvc.perform(MockMvcRequestBuilders.post("/create")
//	            .contentType(MediaType.APPLICATION_JSON)
//	            .content(asJsonString(user)))
//	      		.andExpect(MockMvcResultMatchers.status().isCreated())
//	      		.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1233));
//	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
