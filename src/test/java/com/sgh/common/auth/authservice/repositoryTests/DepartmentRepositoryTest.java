package com.sgh.common.auth.authservice.repositoryTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.repositories.DepartmentRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DepartmentRepositoryTest {
	
	@Autowired
	private DepartmentRepository deptRepository;
	
	@Test
	public void shouldReturnAllDepartments() {
		Department dept = new Department(1l,"Solutions");
		Department dept2 = new Department(2l,"TechLabs");
		deptRepository.save(dept);
		deptRepository.save(dept2);
		
		List<Department> depts = deptRepository.findAll();
		assertTrue(depts.size()==2);
		assertTrue(depts.contains(deptRepository.findById(dept.getDeptId()).get()));
		assertTrue(depts.contains(deptRepository.findById(2l).get()));
	}
	
	@Test
	public void shouldReturnDepartmentWithRelevantName() {
		Department dept = new Department(3l,"Operations");
		Department dept2 = new Department(4l,"HR");
		deptRepository.save(dept);
		deptRepository.save(dept2);
		
		Department returnedDept = deptRepository.findByDeptName(dept.getDeptName()).get();
		assertEquals(dept.getDeptId(), returnedDept.getDeptId());
		assertEquals(dept.getDeptName(), returnedDept.getDeptName());
	}

}
