package com.sgh.common.auth.authservice.serviceTests;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.repositories.RoleRepository;
import com.sgh.common.auth.authservice.services.RoleService;
import com.sgh.common.auth.authservice.services.RoleServiceImpl;

@ExtendWith(SpringExtension.class)
public class TestRoleService {
	
	@MockBean
	private RoleRepository roleRepository;
	
	@TestConfiguration
	static class TestUserServiceContextConfiguration {
		
		@Bean
		public RoleService roleService() {
			return new RoleServiceImpl();
		}
	}
	
	@Autowired
	private RoleService roleService;
	
	@Test
	public void shouldReturnRoleWithRelevantName() {
		Role role = new Role(1, ERole.MANAGER);
		Mockito.when(roleRepository.findByName(role.getName())).thenReturn(Optional.of(role));
		
		Role returnedRole = roleService.getRoleByName(role.getName().toString());
		assertEquals(role.getId(),returnedRole.getId());
		assertEquals(role.getName(),returnedRole.getName());
		
	}

}
