package com.sgh.common.auth.authservice.repositoryTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.repositories.RoleRepository;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class RoleRepositoryTest {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Test
	public void shouldReturnRoleWithRelevantName() {
		Role role = new Role(1,ERole.MANAGER);
		roleRepository.save(role);
		
		Role returnedRole = roleRepository.findByName(role.getName()).get();
		assertEquals(role.getName(),returnedRole.getName());
	}
	
}
