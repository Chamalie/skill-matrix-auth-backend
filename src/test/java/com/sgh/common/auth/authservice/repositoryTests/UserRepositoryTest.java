package com.sgh.common.auth.authservice.repositoryTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.models.SmUser;
import com.sgh.common.auth.authservice.repositories.DepartmentRepository;
import com.sgh.common.auth.authservice.repositories.UserRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	DepartmentRepository deptRepository;

	@Test
	public void shouldReturnUserFromUsername() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1, ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1234,"user2","pass",role,"f","l",dept,supervisor, true);
		userRepository.save(supervisor);
		userRepository.save(user);

		SmUser returnedUser = userRepository.findByUsername(user.getUsername()).get();
		assertEquals(user.getUsername(), returnedUser.getUsername());

	}
 
	@Test
	public void shouldReturnTrueForExistingUser() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1, ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1235,"user3","pass",role,"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1236,"user4","pass",role,"f","l",dept,supervisor, true);
		userRepository.save(supervisor);
		userRepository.save(user);

		assertTrue(userRepository.existsByUsername(user.getUsername()));
	}

	@Test
	public void shouldSaveUser() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1, ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1237,"user5","pass",role,"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1238,"user6","pass",role,"f","l",dept,supervisor, true);
		userRepository.save(supervisor);
		SmUser savedUser = userRepository.save(user);
		assertTrue(userRepository.existsById(savedUser.getEmployeeId()));
	}

	@Test
	public void shouldReturnAllUsers() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1, ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1235,"user2","pass",role,"f","l",dept,supervisor, true);
		userRepository.save(supervisor);
		userRepository.save(user);

		List<SmUser> users = userRepository.findAll();
		assertTrue(users.size()==2);
	}
	
	@Test
	public void shouldReturnAllUsersUnderGivenSupervisor() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1,ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1233,"user1","pass",new Role(2, ERole.MANAGER),"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1235,"user2","pass",role,"f","l",dept,supervisor, true);
		SmUser user2 = new SmUser((long) 1236,"user3","pass",role,"f","l",dept,supervisor, true);
		SmUser user3 = new SmUser((long) 1237,"user4","pass",role,"f","l",dept,supervisor, true);
		SmUser user4 = new SmUser((long) 1238,"user5","pass",role,"f","l",dept,user, true);
		userRepository.save(supervisor);
		userRepository.save(user);
		userRepository.save(user2);
		userRepository.save(user3);
		userRepository.save(user4);
		
		List<SmUser> users = userRepository.findBySupervisor(supervisor);
		assertTrue(users.size()==3);
		assertTrue(users.contains(userRepository.findByEmployeeId(user.getEmployeeId()).get()));
		assertTrue(users.contains(userRepository.findByEmployeeId(user2.getEmployeeId()).get()));
		assertTrue(users.contains(userRepository.findByEmployeeId(user3.getEmployeeId()).get()));	
	}
	
	@Test
	public void shouldReturnAllManagers() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1,ERole.MANAGER);
		Role role2 =new Role(2, ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1235,"user2","pass",role2,"f","l",dept,supervisor, true);
		SmUser user2 = new SmUser((long) 1236,"user3","pass",role,"f","l",dept,supervisor, true);
		SmUser user3 = new SmUser((long) 1237,"user4","pass",role,"f","l",dept,supervisor, true);
		SmUser user4 = new SmUser((long) 1238,"user5","pass",role2,"f","l",dept,user, true);
		userRepository.save(supervisor);
		userRepository.save(user);
		userRepository.save(user2);
		userRepository.save(user3);
		userRepository.save(user4);
		
		List<SmUser> users = userRepository.findByRole(role);
		assertTrue(users.size()==3);
		assertTrue(!(users.contains(userRepository.findByEmployeeId(user.getEmployeeId()).get())));
		assertTrue(users.contains(userRepository.findByEmployeeId(user2.getEmployeeId()).get()));
		assertTrue(users.contains(userRepository.findByEmployeeId(user3.getEmployeeId()).get()));	
	}
	
	@Test
	public void shouldReturnAllUsersInGivenDepartment() {
		Department dept = new Department(1l,"Solutions");
		Department dept2 = new Department(2l,"TechLabs");
		deptRepository.save(dept);
		deptRepository.save(dept2);
		Role role = new Role(1,ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1233,"user1","pass",new Role(2, ERole.MANAGER),"f","l",dept, null, true);
		SmUser user = new SmUser((long) 1235,"user2","pass",role,"f","l",dept,supervisor, true);
		SmUser user2 = new SmUser((long) 1236,"user3","pass",role,"f","l",dept2,supervisor, true);
		SmUser user3 = new SmUser((long) 1237,"user4","pass",role,"f","l",dept,supervisor, true);
		userRepository.save(supervisor);
		userRepository.save(user);
		userRepository.save(user2);
		userRepository.save(user3);

		List<SmUser> users1 = userRepository.findAll();
		System.out.println("################################################# \n" + users1);
		List<SmUser> users = userRepository.findByDepartment(dept);
		System.out.println("################################################# \n" + users);
		//assertTrue(users.size()==3);
		assertTrue(users.contains(userRepository.findByEmployeeId(user.getEmployeeId()).get()));
		assertTrue(!(users.contains(userRepository.findByEmployeeId(user2.getEmployeeId()).get())));
		assertTrue(users.contains(userRepository.findByEmployeeId(user3.getEmployeeId()).get()));	
	}

}
