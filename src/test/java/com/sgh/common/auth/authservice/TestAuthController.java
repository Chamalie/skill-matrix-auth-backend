package com.sgh.common.auth.authservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sgh.common.auth.authservice.models.JwtRequest;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TestAuthController {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void givenExistingUserReturnAuthenticatedWithJwt() throws Exception {
		JwtRequest jwtRequest = new JwtRequest("user1", "emp_pass");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/login")
				.contentType(MediaType.APPLICATION_JSON)
	            .content(asJsonString(jwtRequest)))
	            .andExpect(MockMvcResultMatchers.status().isOk())
	            .andExpect(MockMvcResultMatchers.header().exists("Authorization"));
	}
	
	@Test
	public void givenExistingManagerReturnAuthenticatedWithJwtContainingRole() throws Exception {
		JwtRequest jwtRequest = new JwtRequest("user1", "emp_pass");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/login")
				.contentType(MediaType.APPLICATION_JSON)
	            .content(asJsonString(jwtRequest)))
	            .andExpect(MockMvcResultMatchers.status().isOk())
	            .andExpect(MockMvcResultMatchers.header().exists("Authorization"));
	}
	
	@Test
	public void givenNonExistingUsernameReturnError() throws Exception {
		JwtRequest jwtRequest = new JwtRequest("nonExistingUser", "emp_pass");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/login")
				.contentType(MediaType.APPLICATION_JSON)
	            .content(asJsonString(jwtRequest)))
	            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
	            .andExpect(MockMvcResultMatchers.header().doesNotExist("Authorization"));
		
	}
	
	@Test
	public void givenIncorrectPasswordShouldReturnError() throws Exception {
		JwtRequest jwtRequest = new JwtRequest("user1", "wrongPassword");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/login")
				.contentType(MediaType.APPLICATION_JSON)
	            .content(asJsonString(jwtRequest)))
	            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
	            .andExpect(MockMvcResultMatchers.header().doesNotExist("Authorization"));
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
