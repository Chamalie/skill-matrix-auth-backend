package com.sgh.common.auth.authservice.serviceTests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.models.SmUser;
import com.sgh.common.auth.authservice.repositories.DepartmentRepository;
import com.sgh.common.auth.authservice.repositories.RoleRepository;
import com.sgh.common.auth.authservice.repositories.UserRepository;
import com.sgh.common.auth.authservice.services.DepartmentService;
import com.sgh.common.auth.authservice.services.DepartmentServiceImpl;
import com.sgh.common.auth.authservice.services.RoleService;
import com.sgh.common.auth.authservice.services.RoleServiceImpl;
import com.sgh.common.auth.authservice.services.UserService;
import com.sgh.common.auth.authservice.services.UserServiceImpl;

@ExtendWith(SpringExtension.class)
public class TestUserService {
	
	@MockBean
	private UserRepository userRepository;
	
	@MockBean 
	private DepartmentRepository deptRepository;
	
	@MockBean
	private RoleRepository roleRepository;
	
	@TestConfiguration
	static class TestUserServiceContextConfiguration {
		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}
		
		@Bean
		public DepartmentService deptService() {
			return new DepartmentServiceImpl();
		}
		
		@Bean
		public RoleService roleService() {
			return new RoleServiceImpl();
		}
	}

	@Autowired
	private UserService userService;
	
	@Test
	public void shouldCreateUserAndReturnSameUser() {
		Department dept = new Department(1l, "Solutions");
		Role role = new Role(1,ERole.EMPLOYEE);
		SmUser user = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		SmUser savedUser = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		Mockito.when(deptRepository.findByDeptName(dept.getDeptName())).thenReturn(Optional.of(dept));
		Mockito.when(roleRepository.findByName(role.getName())).thenReturn(Optional.of(role));
		Mockito.when(userRepository.save(user)).thenReturn(savedUser);
		
		SmUser returnedUser = userService.createUser(user);
		assertEquals(savedUser.getEmployeeId(), returnedUser.getEmployeeId());
		assertEquals(savedUser.getUsername(), returnedUser.getUsername());
		assertEquals(savedUser.getPassword(), returnedUser.getPassword());
	}
	
	@Test
	public void shouldReturnAllUsers() {
		Department dept = new Department("Solutions");
		Role role = new Role(ERole.EMPLOYEE);
		List <SmUser> users = new ArrayList<SmUser>();
		users.add(new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true));
		users.add(new SmUser((long)1234,"user2","pass",role,"f","l",dept, null, true));
		users.add(new SmUser((long)1235,"user3","pass",role,"f","l",dept, null, true));
		Mockito.when(userRepository.findAll()).thenReturn(users);
		
		List <SmUser> returnedUsers = userService.getAllUsers();
		assertEquals(users, returnedUsers);
	}
	
	@Test
	public void shouldReturnUserWithGivenId() {
		Department dept = new Department("Solutions");
		Role role = new Role(ERole.EMPLOYEE);
		SmUser user = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		SmUser expectedUser = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		Mockito.when(userRepository.findByEmployeeId(user.getEmployeeId())).thenReturn(Optional.of(expectedUser));
		
		SmUser returnedUser = userService.getUser(user.getEmployeeId());
		assertEquals(expectedUser.getEmployeeId(), returnedUser.getEmployeeId());
		assertEquals(expectedUser.getUsername(), returnedUser.getUsername());
		assertEquals(expectedUser.getPassword(), returnedUser.getPassword());
	}
	
	@Test
	public void shouldUpdateUserAndReturnUpdatedUser() {
		Department dept = new Department(1l,"Solutions");
		Role role = new Role(1, ERole.EMPLOYEE);
		SmUser user = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		SmUser savedUser = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		Mockito.when(deptRepository.findByDeptName(dept.getDeptName())).thenReturn(Optional.of(dept));
		Mockito.when(roleRepository.findByName(role.getName())).thenReturn(Optional.of(role));
		Mockito.when(userRepository.save(user)).thenReturn(savedUser);
		
		SmUser updatedUser = userService.update(user);
		assertEquals(savedUser.getEmployeeId(), updatedUser.getEmployeeId());
		assertEquals(savedUser.getUsername(), updatedUser.getUsername());
		assertEquals(savedUser.getPassword(), updatedUser.getPassword());
	}
	
	@Test
	public void shouldDeactivateUser() {
		Department dept = new Department("Solutions");
		Role role = new Role(ERole.EMPLOYEE);
		SmUser deactivatedUser = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, false);
		SmUser returnedUser = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, false);
		Mockito.when(userRepository.findByEmployeeId(deactivatedUser.getEmployeeId()))
		.thenReturn(Optional.of(deactivatedUser));
		Mockito.when(userRepository.save(deactivatedUser)).thenReturn(returnedUser);
		
		SmUser userToDeactivate = new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true);
		userService.deactivateUser(userToDeactivate.getEmployeeId());
		Mockito.verify(userRepository, Mockito.times(1)).findByEmployeeId(deactivatedUser.getEmployeeId());
		Mockito.verify(userRepository, Mockito.times(1)).save(deactivatedUser);
	}
	
	@Test
	public void shouldReturnUsersBelongingToGivenDepartment() {
		Department dept = new Department("Solutions");
		Role role = new Role(ERole.EMPLOYEE);
		List <SmUser> users = new ArrayList<SmUser>();
		users.add(new SmUser((long)1233,"user1","pass",role,"f","l",dept, null, true));
		users.add(new SmUser((long)1234,"user2","pass",role,"f","l",dept, null, true));
		users.add(new SmUser((long)1235,"user3","pass",role,"f","l",dept, null, true));
		Mockito.when(deptRepository.findByDeptName("Solutions")).thenReturn(Optional.of(dept));
		Mockito.when(userRepository.findByDepartment(dept)).thenReturn(users);
		
		List <SmUser> returnedUsers = userService.getUsersByDepartment("Solutions");
		assertEquals(users, returnedUsers);
	}
	
	@Test
	public void shouldReturnUsersUnderGivenSupervisor() {
		Department dept = new Department("Solutions");
		Role role = new Role(ERole.EMPLOYEE);
		SmUser supervisor = new SmUser((long)1232,"user","pass",role,"f","l",dept, null, true);
		List <SmUser> users = new ArrayList<SmUser>();
		users.add(new SmUser((long)1233,"user1","pass",role,"f","l",dept, supervisor, true));
		users.add(new SmUser((long)1234,"user2","pass",role,"f","l",dept, supervisor, true));
		users.add(new SmUser((long)1235,"user3","pass",role,"f","l",dept, supervisor, true));
		Mockito.when(userRepository.findByUsername("user")).thenReturn(Optional.of(supervisor));
		Mockito.when(userRepository.findBySupervisor(supervisor)).thenReturn(users);
		
		List <SmUser> returnedUsers = userService.getUsersBySupervisor("user");
		assertEquals(users, returnedUsers);
	}
	
	@Test
	public void shouldReturnAllManagers() {
		Department dept = new Department("Solutions");
		Role role = new Role(1, ERole.MANAGER);
		SmUser supervisor = new SmUser((long)1232,"user","pass",role,"f","l",dept, null, true);
		List <SmUser> users = new ArrayList<SmUser>();
		users.add(new SmUser((long)1233,"user1","pass",role,"f","l",dept, supervisor, true));
		users.add(new SmUser((long)1234,"user2","pass",role,"f","l",dept, supervisor, true));
		users.add(new SmUser((long)1235,"user3","pass",role,"f","l",dept, supervisor, true));
		Mockito.when(roleRepository.findByName(role.getName())).thenReturn(Optional.of(role));
		Mockito.when(userRepository.findByRole(role)).thenReturn(users);
		
		List <SmUser> returnedUsers = userService.getAllManagers();
		System.out.println("##########################################" + returnedUsers);
		assertEquals(users, returnedUsers);
	}
	
	
}
