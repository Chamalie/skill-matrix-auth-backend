package com.sgh.common.auth.authservice.serviceTests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.repositories.DepartmentRepository;
import com.sgh.common.auth.authservice.services.DepartmentService;
import com.sgh.common.auth.authservice.services.DepartmentServiceImpl;

@ExtendWith(SpringExtension.class)
public class TestDepartmentService {
	
	@MockBean 
	private DepartmentRepository deptRepository;
	
	@TestConfiguration
	static class TestUserServiceContextConfiguration {
		
		@Bean
		public DepartmentService deptService() {
			return new DepartmentServiceImpl();
		}
	}
	
	@Autowired
	private DepartmentService deptService;
	
	@Test
	public void shouldReturnAllDepartments() {
		Department dept = new Department(1l, "Solutions");
		Department dept2 = new Department(2l, "Operations");
		Department dept3 = new Department(1l, "HR");
		List<Department> departments = new ArrayList<Department>();
		departments.add(dept);
		departments.add(dept2);
		departments.add(dept3);
		Mockito.when(deptRepository.findAll()).thenReturn(departments);
		
		List<Department> returnedDepts = deptService.getAllDepartments();
		assertEquals(departments, returnedDepts);
		
	}
	
	@Test
	public void shouldReturnDepartmentWithRelevantName() {
		Department dept = new Department(1l, "Solutions");
		Mockito.when(deptRepository.findByDeptName(dept.getDeptName())).thenReturn(Optional.of(dept));
		
		Department returnedDept = deptService.getDepartmentByName(dept.getDeptName());
		assertEquals(dept.getDeptId(),returnedDept.getDeptId());
		assertEquals(dept.getDeptName(),returnedDept.getDeptName());
		
	}

}
