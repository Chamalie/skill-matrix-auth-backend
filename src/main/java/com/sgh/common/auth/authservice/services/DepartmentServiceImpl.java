package com.sgh.common.auth.authservice.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.repositories.DepartmentRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	
	@Autowired
	private DepartmentRepository deptRepository;

	@Override
	public List<Department> getAllDepartments() {
		return deptRepository.findAll();
	}

	@Override
	public Department getDepartmentByName(String deptName) {
		return deptRepository.findByDeptName(deptName).get();
	}

}
