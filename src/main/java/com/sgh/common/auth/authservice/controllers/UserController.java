package com.sgh.common.auth.authservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgh.common.auth.authservice.models.SmUser;
import com.sgh.common.auth.authservice.services.UserService;

@RestController
@CrossOrigin()
public class UserController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody SmUser user) throws Exception {
		try {
			SmUser savedUser = userService.createUser(user);
			return new ResponseEntity<SmUser>(savedUser, HttpStatus.CREATED);
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getStackTrace(),HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable("id") Long id) throws Exception {
		try {
			SmUser user = userService.getUser(id);
			return new ResponseEntity<SmUser>(user, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	 
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<?> getAllUsers() throws Exception {
		try {
			List<SmUser> users = userService.getAllUsers();
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody SmUser user) throws Exception {
		try {
			SmUser updatedUser = userService.update(user);
			return new ResponseEntity<SmUser>(updatedUser, HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getStackTrace(),HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> deactivate(@PathVariable("id") Long id) throws Exception {
		try {
			userService.deactivateUser(id);
			return new ResponseEntity<SmUser>(HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getStackTrace(),HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/dept/{deptName}", method = RequestMethod.GET)
	public ResponseEntity<?> getDepartmentUsers(@PathVariable("deptName") String deptName) throws Exception {
		try {
			List<SmUser> users = userService.getUsersByDepartment(deptName);
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/supervisor/{supervisor}", method = RequestMethod.GET)
	public ResponseEntity<?> getUsersUnderSupervisor(@PathVariable("supervisor") String supervisor) throws Exception {
		try {
			List<SmUser> users = userService.getUsersBySupervisor(supervisor);
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/managers", method = RequestMethod.GET)
	public ResponseEntity<?> getManagers() throws Exception {
		try {
			List<SmUser> managers = userService.getAllManagers();
			return new ResponseEntity<>(managers, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

}
