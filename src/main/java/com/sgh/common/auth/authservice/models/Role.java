package com.sgh.common.auth.authservice.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
//@Table(uniqueConstraints = { @UniqueConstraint(columnNames = "name") })
public class Role {
	
	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column
	private ERole name;
	
	public Role() {
		
	}
	
	public Role(ERole name) {
		this.name = name;
	}
	
	public Role(String name) {
		this.name = ERole.valueOf(name);
	}
	
	public Role(Integer id, ERole name) {
		this.id =id;
		this.name = name;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ERole getName() {
		return name;
	}

	public void setName(ERole name) {
		this.name = name;
	}
	
	

}
