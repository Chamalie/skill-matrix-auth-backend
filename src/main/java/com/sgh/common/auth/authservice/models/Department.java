package com.sgh.common.auth.authservice.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Department {
	
	@Id
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long deptId;
	
	@Column(nullable=false)
	private String deptName;
	
//	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
//    private List<SmUser> employees = new ArrayList<SmUser>();

	public Department() {
		
	} 

	public Department(String deptName) {
		super();
		this.deptName = deptName;
	}
	
	public Department(Long deptId, String deptName) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
	}

	public Long getDeptId() {
		return deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

//	public List<SmUser> getEmployees() {
//		return employees;
//	}
//
//	public void setEmployees(List<SmUser> employees) {
//		this.employees = employees;
//	}
	

}
