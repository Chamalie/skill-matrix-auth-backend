package com.sgh.common.auth.authservice.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sgh.common.auth.authservice.models.JwtUser;
import com.sgh.common.auth.authservice.models.SmUser;
import com.sgh.common.auth.authservice.repositories.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SmUser user = userRepository.findByUsername(username).get();
		if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(user.getRole().getName().toString()));
		return new JwtUser(user.getUsername(), user.getPassword(), true, true, true, true, authorities, user.getEmployeeId());
	}
}
 