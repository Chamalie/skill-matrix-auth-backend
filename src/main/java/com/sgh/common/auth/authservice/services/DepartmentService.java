package com.sgh.common.auth.authservice.services;

import java.util.List;

import com.sgh.common.auth.authservice.models.Department;

public interface DepartmentService {
	
	public List<Department> getAllDepartments();
	
	public Department getDepartmentByName(String deptName);

}
