package com.sgh.common.auth.authservice.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = 439471433845539068L;
	
	private String jwtToken;
	
	public JwtResponse(String jwtToken) {
		this.jwtToken = jwtToken;
	}
	
	public String getJwtToken() {
		return jwtToken;
	}

}
