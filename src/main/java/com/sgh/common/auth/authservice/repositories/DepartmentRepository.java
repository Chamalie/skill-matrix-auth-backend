package com.sgh.common.auth.authservice.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgh.common.auth.authservice.models.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
	
	public Optional<Department> findByDeptName(String deptName);
	
}
