package com.sgh.common.auth.authservice.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

	public Optional<Role> findByName(ERole name);

}
