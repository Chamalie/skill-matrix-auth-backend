package com.sgh.common.auth.authservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.models.SmUser;

@Repository
public interface UserRepository extends JpaRepository<SmUser, Long> {
	
	public Optional<SmUser> findByUsername(String username);
	
	public boolean existsByUsername(String username);
	
	public Optional<SmUser> findByEmployeeId(Long employeeId);
	
	public List<SmUser> findByDepartment(Department department);
	
	public List<SmUser> findBySupervisor(SmUser supervisor);
	
	public List<SmUser> findByRole(Role role);
	
}