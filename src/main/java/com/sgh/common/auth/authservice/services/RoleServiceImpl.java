package com.sgh.common.auth.authservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.repositories.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role getRoleByName(String name) {
		return roleRepository.findByName(ERole.valueOf(name)).get();
	}

}
