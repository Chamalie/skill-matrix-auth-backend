package com.sgh.common.auth.authservice.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Where;

@Entity
@Where(clause="is_active=1")
@Table(name = "sm_user", uniqueConstraints = { @UniqueConstraint(columnNames = "username") })
public class SmUser {

	@Id
	private Long employeeId;

	@Column(nullable=false)
	private String username;

	@Column(nullable=false)
	private String password;

	@ManyToOne(cascade = CascadeType.ALL)
	private Role role;
	
	@Column(nullable=false)
	private String firstName;
	
	@Column(nullable=false)
	private String lastName;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dept_id", nullable = false)
	private Department department;
	
	@ManyToOne
	private SmUser supervisor;
	
	@Column(name="is_active", columnDefinition = "boolean default true")
	private Boolean active;
	
	public SmUser() {
		
	}
	
	public SmUser(int id) {
		this.employeeId = Long.valueOf(id);	
	}
	
	public SmUser(String username) {
		this.username = username;	
	}
	
	public SmUser(Long employeeId, String username, String password, Role role, String firstName, String lastName,
			Department department, SmUser supervisor, Boolean active) {
		super();
		this.employeeId = employeeId;
		this.username = username;
		this.password = password;
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
		this.department = department;
		this.supervisor = supervisor;
		this.active = active;
	}


	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public SmUser getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(SmUser supervisor) {
		this.supervisor = supervisor;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
}	
	
	
	