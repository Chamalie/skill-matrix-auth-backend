package com.sgh.common.auth.authservice.models;

public enum ERole {
	EMPLOYEE, 
	MANAGER,
	ADMIN
}
