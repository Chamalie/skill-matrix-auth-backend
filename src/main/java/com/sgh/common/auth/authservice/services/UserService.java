package com.sgh.common.auth.authservice.services;

import java.util.List;

import com.sgh.common.auth.authservice.models.SmUser;

public interface UserService {

	public SmUser createUser(SmUser user);

	public List<SmUser> getAllUsers();

	public SmUser getUser(long employeeId);

	public SmUser update(SmUser user);

	public void deactivateUser(long employeeId);
	
	public List<SmUser> getUsersByDepartment(String department);
	
	public List<SmUser> getUsersBySupervisor(String supervisor);
	
	public List<SmUser> getAllManagers();
}
