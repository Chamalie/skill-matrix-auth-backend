package com.sgh.common.auth.authservice.services;

import com.sgh.common.auth.authservice.models.Role;

public interface RoleService {
	
	public Role getRoleByName(String name);
}
