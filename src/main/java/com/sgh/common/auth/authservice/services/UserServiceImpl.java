package com.sgh.common.auth.authservice.services;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.models.ERole;
import com.sgh.common.auth.authservice.models.Role;
import com.sgh.common.auth.authservice.models.SmUser;
import com.sgh.common.auth.authservice.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    UserRepository userRepository;
	
	@Autowired
	DepartmentService deptService;
	
	@Autowired
	RoleService roleService;

	@Override
	public SmUser createUser(SmUser user) throws NoSuchElementException {
		String password = user.getPassword();
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
        user.setPassword(encodedPassword);
        
        String deptName = user.getDepartment().getDeptName();
        Department dept = deptService.getDepartmentByName(deptName);
        user.setDepartment(dept);
        
        String roleName = user.getRole().getName().toString();
        Role role = roleService.getRoleByName(roleName);
        user.setRole(role);
        
        if (user.getSupervisor()!= null) {
        	if (user.getSupervisor().getUsername()!=null && 
        			userRepository.existsByUsername(user.getSupervisor().getUsername())) {
        		String supervisorName = user.getSupervisor().getUsername();
            	SmUser supervisor = userRepository.findByUsername(supervisorName).get();
            	user.setSupervisor(supervisor);
        	}
        }
		return userRepository.save(user);
	}

	@Override
	public List<SmUser> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public SmUser getUser(long employeeId) {
		return userRepository.findByEmployeeId(employeeId).get();
	}

	@Override
	public SmUser update(SmUser user) {
		String password = user.getPassword();
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
        user.setPassword(encodedPassword);
        
        String deptName = user.getDepartment().getDeptName();
        Department dept = deptService.getDepartmentByName(deptName);
        user.setDepartment(dept);
        
        String roleName = user.getRole().getName().toString();
        Role role = roleService.getRoleByName(roleName);
        user.setRole(role);
        
        if (user.getSupervisor()!= null) {
        	if (user.getSupervisor().getUsername()!=null&& 
        			userRepository.existsByUsername(user.getSupervisor().getUsername())) {
        		String supervisorName = user.getSupervisor().getUsername();
            	SmUser supervisor = userRepository.findByUsername(supervisorName).get();
            	user.setSupervisor(supervisor);
        	}
        }
		return userRepository.save(user);
	}

	@Override
	public void deactivateUser(long employeeId) {
		SmUser user = userRepository.findByEmployeeId(employeeId).get();
		user.setActive(false);
		userRepository.save(user);
	}

	@Override
	public List<SmUser> getUsersByDepartment(String deptName) {
		Department dept = deptService.getDepartmentByName(deptName);
		return userRepository.findByDepartment(dept);
		
	}

	@Override
	public List<SmUser> getUsersBySupervisor(String username) {
		SmUser supervisor = userRepository.findByUsername(username).get();
		return userRepository.findBySupervisor(supervisor);
		
	}
	
	@Override
	public List<SmUser> getAllManagers() {
		Role role = roleService.getRoleByName(ERole.MANAGER.toString());
		return userRepository.findByRole(role);
	}

}
