package com.sgh.common.auth.authservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgh.common.auth.authservice.models.Department;
import com.sgh.common.auth.authservice.services.DepartmentService;

@RestController
@CrossOrigin()
public class DeptController {
	
	@Autowired
	private DepartmentService deptService;
	
	@RequestMapping(value = "/departments", method = RequestMethod.GET)
	public ResponseEntity<?> getAllDepartments() throws Exception {
		try {
			List<Department> depts = deptService.getAllDepartments();
			return new ResponseEntity<>(depts, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

}
